# Frontend Dev Quiz



# Quiz:

Existen N grifos a lo largo de una ruta circular, donde la cantidad de gasolina en el grifo i, es grifo[i].

Tienes un auto con un tanque de gasolina ilimitado y este tiene un costo de costo[i] de gasolina para viajar desde el grifo i al siguiente grifo (i + 1). 
Comienzas el viaje con un tanque vacío en uno de los grifos.

Devuelve el índice de la gasolinera inicial si puedes recorrer el circuito una vez en el sentido de las agujas del reloj; de lo contrario, devuelve -1.

### Notas:

Si existe una solución, se garantiza que será única.
Ambas matrices de entrada no están vacías y tienen la misma longitud.
Cada elemento de las matrices de entrada es un número entero no negativo.

### Ejemplo 1

```
Parametros de entrada:
grifo = [1,2,3,4,5]
costo = [3,4,5,1,2]

Parametro devuelto: 3
```

> Explicación:
> Iniciando en la estación 3 (índice 3) y llena con 4 unidades de gas (grifo[3]). Tu tanque = 0 + 4 = 4
Viaja al grifo 4. Tu tanque = 4 - 1 + 5 = 8
Viaja al grifo 0. Tu tanque = 8 - 2 + 1 = 7
Viaja al grifo 1. Tu tanque = 7 - 3 + 2 = 6
Viaja al grifo 2. Tu tanque = 6 - 4 + 3 = 5
Viaje al grifo 3. El costo es 5. Su gasolina es suficiente para viajar de regreso al grifo 3.
Por lo tanto, devuelve 3 como índice inicial.


### Ejemplo 2

```js
# Parametros de entrada:
grifo = [2,3,4]
costo = [3,4,3]

# Parametro devueltos: -1
```
> Explicación: 
> No puede comenzar en el grifo 0 o 1, ya que no hay suficiente gasolina para viajar al siguiente.
Comencemos en el grifo 2 y llenamos con 4 unidades de gasolina. Tu tanque = 0 + 4 = 4
Viaja al grifo 0. Tu tanque = 4 - 3 + 2 = 3
Viaja al grifo 1. Tu tanque = 3 - 3 + 3 = 3
No puede viajar de regreso al grifo 2, ya que requiere 4 unidades de gasolina pero solo tiene 3.
Por lo tanto, no puede recorrer el circuito una vez, sin importar dónde comience.


## Recomendaciones:

- Utiliza react, typescript y el stack complementario que consideres.
- La interfaz debe permitir ingresar los parámetros de grifo y costo, los valores irán separados por coma, deberán ser enteros y asignarlos a los array de entrada.
- El parámetro devuelto debe ser mostrado en la interfaz.
- La respuesta deberá ser enviada en un repositorio de github al correo jdiaz@zutun.io con el asunto Dev Quiz incluyendo su nombre: "Dev Quiz - Jean Diaz".
- Intente usar buenas practicas de programación y separar sus avances en commits.
- Do your best!


> 💡 Siente libre de realizar cualquier duda o consulta al correo jdiaz@zutun.io y/o por whatsapp al 📱 +51989712993

